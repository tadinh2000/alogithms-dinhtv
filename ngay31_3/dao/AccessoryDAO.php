<?php
require_once('./../abstract/BaseDAO.php');

class AccessoryDao extends BaseDao
{
    /**
     * get Accessory by Name
     * @param $name
     * @return mixed
     */
    public function findByName( $name)
    {
        return $this->database->getTableByName('accessoryTable', $name);
    }

    /**
     * get Accessory where
     * @return mixed
     */
    public function search()
    {
        return $this->database->selectTable('accessoryTable');
    }
}