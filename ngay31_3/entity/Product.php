<?php
require_once('./../abstract/BaseRow.php');
class Product extends BaseRow{

    public $categoryID;
    public function __construct($id, $name, $categoryID){
        $this -> id = $id;
        $this -> name = $name;
        $this -> categoryID = $categoryID;
    }
}