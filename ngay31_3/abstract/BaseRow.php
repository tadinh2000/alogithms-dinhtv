<?php
require_once('./../interface/IEnity.php');

abstract class BaseRow implements IEnity
{
    protected $id;
    protected $name;

    public function __construct($id, $name)
    {
       $this->id = $id;
       $this->name = $name;
    }

    /**
     * Get ID by Row
     * @return id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get Name by Row
     * @return name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Id for Row
     * @return void
     */
    protected function setId($id)
    {
        $this->id = $id;
    }
}