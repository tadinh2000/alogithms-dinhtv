<?php
require_once('./../entity/Accessory.php');

class AccessoryDemo extends Accessory
{
    public function __construct()
    {
       
    }

    /**
     * Create instants Accessory
     * @param  $id
     * @param  $name
     * return mixed
     */
    public function createAccessoryTest($id, $name)
    {
        $accessory =  new Accessory($id, $name);
        return $accessory;
    }

    /**
     * Print Accessory
     * @param Accessory $accessory
     * return mixed
     */
    public function printAccessory(Accessory $accessory)
    {
        echo 'Accessory ID: '.$accessory->getId().'<br/>';
        return $accessory;
    }
}
