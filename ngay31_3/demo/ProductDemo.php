<?php
require_once ('../entity/Product.php');
class ProductDemo extends Product {
    public function __construct()
    {
        
    }
    /**
     * Create instants Product
     * @param  $id
     * @param  $name
     * @param  $categoryId
     * return mixed
     */
    public function createProductTest($id,$name,$categoryID){
        $product = new Product($id,$name,$categoryID);
        return $product;
    }

    /**
     * Create instants Product
     * @param Product $product
     * return mixed
     */
    public function printProduct($product){
        echo $product->getId();
        return $product;
    }
}
$productNew = new ProductDemo(1,'abc',1);
//print_r($productNew->printProduct($productNew));
print_r($productNew->getId());