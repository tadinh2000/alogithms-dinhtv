var example1 = new Vue({
    el: '#example-1',
    data: {
        items: [
            { name: 'Cà phê' },
            { name: 'Trà đặc' },
            { name: 'Bò húc' }
        ]
    }
});

var example2 = new Vue({
    el: '#example-2',
    data: {
        parentMessage: 'Parent',
        items: [
            { name: 'Cà phê' },
            { name: 'Trà đặc' },
            { name: 'Bò húc' }
        ]
    }
});

new Vue({
    el: '#v-for-object',
    data: {
        object: {
            // tất nhiên chúng ta đều biết ông Bành Tổ không phải
            // họ Bành tên Tổ, nhưng đây chỉ là một ví dụ…
            'họ': 'Bành',
            'tên': 'Tổ',
            'tuổi': 800
        }
    }
});

new Vue({
    el: '#evenNumbers',
    data: {
        numbers: [ 1, 2, 3, 4, 5 ]
    },
    // Tạo một computed property trả về mảng đã được lọc hoặc sắp xếp
    computed: {
        evenNumbers: function () {
            return this.numbers.filter(function (number) {
                return number % 2 === 0
            })
        }
    },
    //Trong trường hợp không dùng được computed property ta có thể dùng một phương thức
    methods: {
        even: function (numbers) {
            return numbers.filter(function (number) {
                return number % 2 === 0
            })
        }
    }
});

Vue.component('todo-item', {
    template: '\
        <li>\
            {{ title }}\
            <button v-on:click="$emit(\'remove\')">X</button>\
        </li>\
    ',
    props: ['title']
})
  
new Vue({
    el: '#todo-list-example',
    data: {
        newTodoText: '',
        todos: [
            {
            id: 1,
            title: 'luộc khoai',
            },
            {
            id: 2,
            title: 'cùng chị giã gạo',
            },
            {
            id: 3,
            title: 'thổi cơm'
            },
            {
            id: 4,
            title: 'nhổ cỏ vườn'
            }
        ],  
        nextTodoId: 5
    },
    methods: {
        addNewTodo: function () {
            this.todos.push({
            id: this.nextTodoId++,
            title: this.newTodoText
            })
            this.newTodoText = ''
        }
        }
})