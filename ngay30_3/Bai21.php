<?php
//Theo đệ quy
function calSalary($salary, $n) {
    if ($n <= 0){
        return $salary;
    }
    else $salaryCurent = calSalary($salary, $n - 1) + calSalary($salary, $n - 1) * 10/100;
    return $salaryCurent;
}
echo calSalary(10,2);
//Không theo đệ quy
function calSalary1($salary, $n) {
    for ($i = 1; $i<=$n; $i++){
        $wage = $salary * 110/100;
        $salary = $wage;
    }
    return $wage;
}
echo '<br>';
echo calSalary1(10,2);