// Bài 9:
// - Try-Catch là một khối lệnh dùng để bắt lỗi chương trình trong Javascript.
// - Ta sử dụng Try-Catch khi muốn chương trình không bị dừng lại khi một lệnh nào đố bị lỗi.

var demo = 'Ta Van Dinh'
try {
    if (demo !== 'Ta Van Dinh 1') {
        throw new Error('Day khong phai demo ban dau')
    }
} catch (error) {
    console.log(error.message);
} finally{
    console.log('End program...')
}



var username = 'tavandinh'
var password = 'dinh2000'

try {
    if (username !== 'dinh1234' || password != 'dinh2000') {
        new UserError().throwLogin()
    }
} catch (error) {
    console.log(error.message)
}


// Các ObjectError trong Javascript
// - EvalError -> Lỗi trong hàm Eval
// - RangeError -> Nằm ngoài phạm vi giới hạn của một kiểu dữ liệu nào đó
// - ReferenceError -> Sử dụng một biến chưa được khai báo
// - SyntaxError -> Lỗi về cú pháp
// - URIError -> Lỗi được đưa ra nếu bạn sử dụng các ký tự không hợp lệ trong một hàm URL


//***********************************************//


// Bài 10: Callback trong Javascript
// - Hiểu đơn giản thì hàm callback là một hàm sẽ được gọi bởi một hàm khác.
// - Hiểu phức tạp hơn thì callback một hàm A được truyền vào hàm B thông qua các tham số của hàm B. 
//    Bên trong hàm B sẽ gọi đến hàm A để thực hiện một chức năng nào đó.

// Object chứa hàm callback
var domainInfo = {
    name : 'DevFast',
    setName : function(name){
        // giá trị này sẽ không có tác dụng với key name trong object này
        // nếu như ta sử dụng nó là một callback function
        this.name = name;
    }
};
 
// Hàm có tham số callback
function test(callback){
    callback('Tạ Văn Định');
}
 
// Gọi đến hàm và truyền hàm callback vào
test(domainInfo.setName);
 
// Vẫn kết quả cũ DevFast, tức là hàm callback setName đã ko tác động gì tới thuộc tính name
document.write(domainInfo.name); 
 
// Xuống hàng
document.write('<br/>');
 
// kết quả Tạ Văn Định, tức đối tượng window đã tự tạo ra một key name 
// và giá trị của nó chính là giá trị ta đã sét trong hàm setName
// => this chính là window object
document.write(window.name);


//***********************************************//


// Bài 11: Hàm bind() trong Javascript
// Hàm bind() dùng để gán dữ liệu vào đối tượng this của hàm đang sử dụng.

var blog = {
    domain : "DevFast",
    author : "Tạ Văn Định",
    showWebsite : function (callbackFunction){
        callbackFunction();
    },
    render : function(){
        this.showWebsite(function(){
            console.log(this); // là đối tượng this
            console.log(this.domain); // ok
            console.log(this.author); /// ok
        }.bind(this));
    }
};
 
blog.render();


//***********************************************//


// Bài 12: Hàm call() và apply() trong Javascript

// - Hàm call() dùng để thực thi một hàm nào đó với các tham số truyền vào (nếu có),
//      hàm này được tích hợp sẵn trong các đối tượng là function.
// - Hàm apply có công dụng giống như hàm call, tuy nhiên về cú pháp thì có một chút khác biệt như sau:
//      + Tham số đầu tiên của hàm call() là đối tượng this, tiếp theo chính là các tham số của hàm cần gọi.
//      + Tham số đầu tiên của hàm apply() là đối tượng this, tham số tiếp theo là một mảng chứa các tham số của hàm cần gọi.

// Hàm call()
// Hàm này dùng để xử lý khởi tạo
function initProduct(name, price) {
    this.name = name;
    this.price = price;
}
 
function Food(name, price) {
    // Khởi tạo
    // biến this chính là Food, vì vậy sau khi chạy xong đối tượng Food sẽ có hai
    // thuộc tính là name và price
    initProduct.call(this, name, price);
}
 
function Hat(name, price) {
    // Khởi tạo
    // biến this chính là Food, vì vậy sau khi chạy xong đối tượng Food sẽ có hai
    // thuộc tính là name và price
    initProduct.call(this, name, price);
}
var food = new Food('Trái xoài', 5);
var hat = new Food('Cái mũ', 6);

console.log(food);
console.log(hat);


// Hàm apply()
var sayHello = function(name, message){
    console.log(message + name);
};
 
 
sayHello.call(sayHello, 'Định', ' Xin chào ');
sayHello.apply(sayHello, ['Định', ' Xin chào ']);


//***********************************************//